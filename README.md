# fatos - fasta from minos

## description

Container for piezo[1] to generate a fasta file from minos[2] tb vcf

## installation

    git clone https://gitlab.com/dvolk/fatos

    sudo singularity build fatos.img fatos.def

## running e.g.

    singularity exec fatos.img python3 /usr/bin/minosfasta.py your_vcf.vcf /usr/local/lib/python3.6/dist-packages/piezo-0.0.1-py3.6.egg/config/H37rV_v3.gbk output_fasta.fa

## naming images

    NAME=$(basename `git rev-parse --show-toplevel`)-$(date +"%Y%m%dT%H%M%S")_$(git describe --tags --always --dirty).img

## references

    [1] https://github.com/philipwfowler/piezo
    [2] https://github.com/iqbal-lab-org/clockwork
