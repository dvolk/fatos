import sys
import json

sys.path = [x for x in sys.path if '/home' not in x]

import pandas

kraken2_tab_file = sys.argv[1]

# load kraken tab file
tbl = pandas.read_csv(kraken2_tab_file,
                      sep='\t',
                      header=None,
                      names=['RootFragmentsP','RootFragments','DirectFragments','Rank','NCBI_Taxonomic_ID','Name'])

tbl['Name'] = tbl['Name'].apply(lambda x: x.strip())

# get first row where rank is genus
families = tbl.query('Rank == "F"')
genus1es = tbl.query('Rank == "G1"')

for f in families.iterrows():
    f = f[1]
    if f['Name'] == 'Mycobacteriaceae' and f['RootFragments'] > 100000:
        sys.stderr.write('family Mycobacteriaceae > 100000\n')
        sys.stdout.write('Mycobacteriaceae')
        sys.exit(0)

for g1 in genus1es.iterrows():
    g1 = g1[1]
    if g1['Name'] == 'Mycobacterium tuberculosis complex' and f['RootFragments'] > 100000:
        sys.stderr.write('genus 1 Mycobacterium tuberculosis complex > 100000\n')
        sys.stdout.write('Mycobacteriaceae')
        sys.exit(0)

sys.stdout.write('failed')
