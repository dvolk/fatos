#!/usr/bin/python3

import sys

#sys.path = [x for x in sys.path if '/home' not in x]

vcf = sys.argv[1]
genbank_file = sys.argv[2]
fasta = sys.argv[3]

import gumpy
import copy

reference = gumpy.Genome(genbank_file=genbank_file, name=genbank_file)

sample = copy.deepcopy(reference)

sample.apply_vcf_file(vcf_file=vcf, ignore_filter=True, ignore_status=True)

sample.save_fasta(fasta)

